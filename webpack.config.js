'use strict';

const OfflinePlugin = require('offline-plugin');
const path = require('path');
const webpack = require('webpack');
const WebpackHtmlPlugin = require('html-webpack-plugin');

module.exports = environment => {
  const ifProd = plugin => environment.production ? plugin : null;
  const removeEmpty = array => array.filter(entry => !!entry);

  return {
    bail: !!environment.production,
    context: path.resolve(__dirname, 'src'),
    devServer: {
      compress: true,
      contentBase: path.resolve(__dirname, 'src')
    },
    devtool: environment.production ? 'cheap-module-source-map' : 'eval-source-map',
    entry: {
      index: path.resolve(__dirname, 'src', 'index.ts')
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: [ {
            loader: "style-loader" // creates style nodes from JS strings
          }, {
            loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: "sass-loader" // compiles Sass to CSS
          } ]
        },
        {
          test: /\.ts$/,
          use: [ 'babel-loader', 'ts-loader' ]
        }
      ]
    },
    output: {
      chunkFilename: environment.cache ? '[name].[chunkhash].js' : '[name].js',
      filename: environment.cache ? '[name].[chunkhash].js' : '[name].js',
      path: path.resolve(__dirname, 'dist')
    },
    plugins: removeEmpty([
      ifProd(new webpack.DefinePlugin({
        'process.env': { 'NODE_ENV': 'production '}
      })),
      ifProd(new webpack.optimize.UglifyJsPlugin({
        compress: {
          screw_ie8: true,
          warnings: false
        }
      })),
      new OfflinePlugin({
        events: true,
        ServiceWorker: {
          navigateFallbackURL: '/'
        }
      }),
      new WebpackHtmlPlugin({
        filename: path.resolve(__dirname, 'src', 'index.html'),
        title: 'Progressive Web App & Hardware Experiments'
      })
    ]),
    resolve: {
      extensions: [ '.js', '.ts' ]
    },
    target: 'web',
    watch: !!environment.development
  }
};
