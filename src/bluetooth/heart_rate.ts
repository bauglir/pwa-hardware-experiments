export interface IHeartRateMeasurement {
  contactDetected: boolean;
  energyExpended: number;
  heartRate: number;
  rrIntervals: number[];
}

// From https://github.com/WebBluetoothCG/demos/blob/gh-pages/heart-rate-sensor/heartRateSensor.js
export function parseHeartRateEvent(event: any) {
  // In Chrome 50+, a DataView is returned instead of an ArrayBuffer.
  const value = event.target.value.buffer ? event.target.value : new DataView(event.target.value);
  const result = {} as IHeartRateMeasurement;

  const flags = value.getUint8(0);

  const rate16Bits = flags & 0x1;

  let index = 1;
  if (rate16Bits) {
    result.heartRate = value.getUint16(index, /*littleEndian=*/true);
    index += 2;
  } else {
    result.heartRate = value.getUint8(index);
    index += 1;
  }

  const contactDetected = flags & 0x2;
  const contactSensorPresent = flags & 0x4;
  if (contactSensorPresent) {
    result.contactDetected = !!contactDetected;
  }

  const energyPresent = flags & 0x8;
  if (energyPresent) {
    result.energyExpended = value.getUint16(index, /*littleEndian=*/true);
    index += 2;
  }
  const rrIntervalPresent = flags & 0x10;
  if (rrIntervalPresent) {
    const rrIntervals = [];
    for (; index + 1 < value.byteLength; index += 2) {
      rrIntervals.push(value.getUint16(index, /*littleEndian=*/true));
    }
    result.rrIntervals = rrIntervals;
  }

  return result;
}
