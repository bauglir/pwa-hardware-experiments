import { IHeartRateMeasurement, parseHeartRateEvent } from "./heart_rate";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Subject } from "rxjs/Subject";

export class JabraSportPulse {
  connected$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  heart_rate$: Subject<IHeartRateMeasurement> = new Subject();

  public connect() {
    const device = navigator.bluetooth.requestDevice({
      filters: [ {
        services: [ 'heart_rate' ]
      } ]
    });

    const gatt_server = device.then((device: BluetoothDevice) => {
      device.addEventListener('ongattserverdisconnected', (event) => {
        this.connected$.next(false);
      });

      return device.gatt.connect();
    });

    gatt_server
      .then(server => server.getPrimaryService('heart_rate'))
      .then(service => service.getCharacteristic('heart_rate_measurement'))
      .then(characteristic => {
        characteristic.addEventListener('characteristicvaluechanged', (event) => {
          this.connected$.next(true);
          const heart_rate = parseHeartRateEvent(event);
          this.heart_rate$.next(heart_rate);
        });

        characteristic.startNotifications();
      });
  }
}
