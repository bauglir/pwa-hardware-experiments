/// <reference path="../node_modules/offline-plugin/offline-plugin.d.ts" />

import "bootstrap/scss/bootstrap.scss";

import * as OfflinePluginRuntime from "offline-plugin/runtime";

import { JabraSportPulse } from "./bluetooth";

OfflinePluginRuntime.install({
  onUpdateReady: () => OfflinePluginRuntime.applyUpdate(),
  onUpdated: () => location.reload(),
});

const jsp = new JabraSportPulse();
const hrmConnector = document.querySelector("#hrm_device_connector");
hrmConnector.addEventListener("click", () => jsp.connect());

const heartRateOutput = document.querySelector("#heart_rate_output");
jsp.heart_rate$.subscribe((heartRate) => {
  if (heartRate.contactDetected) {
    heartRateOutput.innerHTML = `${heartRate.heartRate} BPM`;
  } else {
    heartRateOutput.innerHTML = "No heart rate detected";
  }
});

jsp.connected$.subscribe((connected) => {
  hrmConnector.hidden = connected;
  heartRateOutput.hidden = !connected;
});
